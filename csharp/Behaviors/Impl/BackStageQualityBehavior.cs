﻿using csharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Behaviors.Impl
{
    public class BackStageQualityBehavior : IUpdateItem
    {
        public void Update(Item item)
        {
            item.Quality++;
            if (item.SellIn < ItemValueConst.HigestSellInBackStage)
            {
                item.Quality++;
            }
            if (item.SellIn < ItemValueConst.MediaSellInMediaBackStage)
            {
                item.Quality++;
            }
            if (item.SellIn < ItemValueConst.LowestSellInBackStage)
            {
                item.Quality = ItemValueConst.LowestQuality;
            }
            if (item.Quality > ItemValueConst.HigestQuality)
            {
                item.Quality = ItemValueConst.HigestQuality;
            }
        }
    }
}
