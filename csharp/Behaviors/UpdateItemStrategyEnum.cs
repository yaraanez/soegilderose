﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Behaviors
{
    public enum UpdateItemStrategyEnum
    {
        NormalQuantity,
        NormalSellIn,
        AgedBrieQuality,
        Sulfuras,
        BackStageQuality
    }
}
