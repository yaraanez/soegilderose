﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Enums
{
    public class ItemValueConst
    {
        public const int LowestSellIn = 0;
        public const int LowestQuality = 0;
        public const int HigestQuality = 50;
        public const int HigestSellInBackStage = 10;
        public const int MediaSellInMediaBackStage = 5;
        public const int LowestSellInBackStage = 0;
    }
}
