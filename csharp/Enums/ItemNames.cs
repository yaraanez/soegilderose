﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Enums
{
    public class ItemNames
    {
        public const string AgedBrie = "Aged Brie";
        public const string BackstagePassesTAFKAL80ETCconcert = "Backstage passes to a TAFKAL80ETC concert";
        public const string SulfurasHandRagnaros = "Sulfuras, Hand of Ragnaros";

    }
}
